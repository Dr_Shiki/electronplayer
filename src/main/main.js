const path = require('path');
const url = require('url');
const fs = require('fs');
const { app, BrowserWindow, Menu, dialog,ipcMain} = require('electron');

let win;
let tmpCSS;

var winSize = {
    width: 700,
    height: 400
}

var currentCSS;

function createWindow()
{
    win = new BrowserWindow
    (
        {
            webPreferences: {
                nodeIntegration: true
            },
            width: winSize.width,
            height: winSize.height,
            backgroundColor: '#00808080',
            frame: false,
            minHeight: winSize.height,
            minWidth: winSize.width
        }
        )
    win.loadURL(url.format({pathname: './src/index.html',protocol: 'file:',slashes:true}));
    win.webContents.on('did-finish-load', e => {
        fs.readFile((__dirname + '\\style.css').replace('main','css'), 'utf8', async (err, data) => {
          currentCSS = await win.webContents.insertCSS(data);
        });
      });
}

ipcMain.on('quit', (event, arg) => {
    app.quit();
  })

ipcMain.on('changeStyleEvil', (event, arg) => {
    win.webContents.removeInsertedCSS(currentCSS);
    fs.readFile((__dirname + '\\anotherStyle.css').replace('main','css'), 'utf8', async (err, html) => {
      currentCSS = await win.webContents.insertCSS(html);
    });
  })
  ipcMain.on('changeStyleNormal', (event, arg) => {
    win.webContents.removeInsertedCSS(currentCSS);
    fs.readFile((__dirname + '\\style.css').replace('main','css'), 'utf8', async (err, html) => {
      currentCSS = await win.webContents.insertCSS(html);
    });
  })

app.on('ready',createWindow);
app.on('closed',()=>{win = null;});
app.on('window-all-closed',()=>{app.quit();});
