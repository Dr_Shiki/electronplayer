

const Video = "video";
const VideoWrapper = "videoContainer";
const playButton = "play_pause";
const seekBar = "seekBar";
const volumeBar = "volumeBar";
const fullScreenButton = "fullScreen";
const playRate = "speedRate";
const controll = "controll";

var isRun = false;
var size;




function loadVideo(vidPath)
{
    
    document.getElementById(Video).src = vidPath;
    document.getElementById(Video).load();
    isRun = false
    size = document.getElementById(Video).duration;
    document.getElementById(seekBar).value = 0;
    document.getElementById(seekBar).max = size;
    document.getElementById(seekBar).step = size/100;
    document.getElementById(volumeBar).max = 1;
    document.getElementById(volumeBar).step = 0.01;
    document.getElementById(Video).volume = document.getElementById(volumeBar).value;
}


document.getElementById(Video).addEventListener
(
    'canplaythrough',
    e=>
    {
        size = document.getElementById(Video).duration;
        document.getElementById(seekBar).max = size;
        document.getElementById(seekBar).step = size/100;
        document.getElementById(volumeBar).max = 1;
        document.getElementById(volumeBar).step = 0.01;
        document.getElementById(Video).volume = document.getElementById(volumeBar).value;
    }
)
document.getElementById(playButton).addEventListener
(
    'click',
    e=>
    {
        isRun? 
        (()=>
            {
                
                //document.getElementById(playButton).style.backgroundImage = 'url(\'img/play.png\')';
                document.getElementById(playButton).className = 'controlls play';
                document.getElementById(Video).pause();
            }
        )():
        (()=>
        {
            //document.getElementById(playButton).style.backgroundImage ='url(\'img/pause.png\')';
            document.getElementById(playButton).className = 'controlls pause';
            document.getElementById(Video).play();
        }
        )();
        isRun = !isRun;
    }
)

document.getElementById(seekBar).addEventListener
(
    'change',
    e=>
    {
        document.getElementById(Video).currentTime = e.target.value;
        if(isRun)
        {
            document.getElementById(Video).play();
        }
    }
)

document.getElementById(seekBar).addEventListener
(
    'mousedown',
    e=>
    {
        if(isRun)
        {
            document.getElementById(Video).pause();
        }
    }
)

document.getElementById(seekBar).addEventListener
(
    'mouseup',
    e=>
    {
        if(isRun)
        {
            document.getElementById(Video).play();
        }
    }
)

document.getElementById(Video).addEventListener
(
    'timeupdate',
    e=>
    {
        document.getElementById(seekBar).value = document.getElementById(Video).currentTime;
    }
)

document.getElementById(volumeBar).addEventListener
(
    'input',
    e=>
    {
        document.getElementById(Video).volume = e.target.value;
    }
)

document.getElementById(fullScreenButton).addEventListener
(
    'click',
    e=>
    {
        console.log('clicked');
        document.getElementById(Video).requestFullscreen();
    }
)

document.getElementById(playRate).addEventListener
(
    'change',
    e=>
    {
        switch(e.target.value)
        {
            case '1.5':
                document.getElementById(Video).playbackRate = 1.5;
                break;
            case '1':
                document.getElementById(Video).playbackRate = 1;
                break;
            case '0.5':
                document.getElementById(Video).playbackRate = 0.5;
                break;  
        }
    }
)

document.onkeypress = 
    (ev)=>
    {
        console.log(ev.keyCode);
        switch(ev.keyCode)
        {
            case 32:
                document.getElementById(playButton).click();
                break;
            default:
                break;
        }
    }


module.exports = 
{
    runVideo: loadVideo
}