const electron = require('electron');
const {dialog} = electron.remote.require('electron');
const player = require('./includes/player')
const { ipcRenderer } = require('electron')
ipcRenderer.send('requireWinSize','request');


let options = {
  title : "Custom title bar", 
  defaultPath : __dirname,
  buttonLabel : "Custom button",
  filters :[
   {name: 'Movies', extensions: ['mkv', 'avi', 'mp4']},
   {name: 'Custom File Type', extensions: ['as']},
   {name: 'All Files', extensions: ['*']}
  ],
  properties: ['openFile','multiSelections']
 }

const body = 'mainDiv';
var pbOpenFile = "openFile";
var pbQuit = "Close";
var vidControll = "controll";
var mainControll = "mainControll";
var changeStyle = "changeStyle";
var evilStyle = false;

var mouseOnControll = false;
var timerRun = false;
var timerControlWatch;
var showControll = 
()=>
{
  isControllRequire?document.getElementById(vidControll).hidden=false:document.getElementById(vidControll).hidden=true;
  document.getElementById(mainControll).hidden=false;
};
var isControllRequire = false;
var isControlHidden = false;
var hidingTimer;
var isTimerOnRun = false;
var isEvilOn = false;


document.getElementById(body).style.width = ''+window.innerWidth+'px';
document.getElementById(body).style.height = ''+window.innerHeight+'px';

document.getElementById(pbOpenFile).addEventListener
(
  'click',
  (e)=>
  {
    dialog.showOpenDialog(options).then((fPathObj)=>
    {
        player.runVideo(fPathObj.filePaths)
        document.onfocus = true;
        isControllRequire = true;
        showControll();
    });
  }
)



document.getElementById(pbQuit).addEventListener
(
  'click',
  (e)=>
  {
    ipcRenderer.sendSync('quit','Close Now')
  }
)

document.getElementById(changeStyle).addEventListener
(
  'click',
  (e)=>
  {
    if(isEvilOn)
    {
      ipcRenderer.send('changeStyleNormal','Close Now')
      isEvilOn=!isEvilOn;
    }
    else
    {
      ipcRenderer.send('changeStyleEvil','Close Now')
      isEvilOn=!isEvilOn;
    }
  }
)

window.onresize=()=>
  {
    document.getElementById(body).style.width = ''+window.innerWidth+'px';
    document.getElementById(body).style.height = ''+window.innerHeight+'px';
  }

document.addEventListener
(
    'mousemove',
    (e)=>
    {
        if(isControlHidden)
        {
            showControll();
            isControlHidden=false;
        }
        if(isTimerOnRun)
        {
            clearTimeout(hidingTimer);
            isTimerOnRun = false;
        }

        hidingTimer = setTimeout
        (
          ()=>
              {
                document.getElementById(vidControll).hidden = true;
                document.getElementById(mainControll).hidden = true;
                isControlHidden=true;
              },
              2000
        )
        isTimerOnRun = true;
    }


)

